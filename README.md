# pi-token-player

A simple app to allow play specific mp3 files based on RFID cards using the RC522 module on RPI

# Getting started


## Setting up the app as service

You may create a `.service` file on `/etc/systemd/system` with the following content:

```
[Unit]
Description=PI-Token-Player
After=multi-user.target

[Service]
ExecStart=/usr/bin/python3.7 /home/pi/pi-token-player/src/app.py
User=pi
WorkingDirectory=/home/pi/pi-token-player/src

[Install]
WantedBy=multi-user.target
```