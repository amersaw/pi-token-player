import os
import time
import subprocess


class MediaPlayerWrapper:
    def __init__(self):
        pass

    def play_file(self, file_path, stop_other_files=True):
        if stop_other_files:
            os.system("killall -9 omxplayer.bin")
            time.sleep(0.5)
        self.command = subprocess.Popen(['omxplayer', file_path])