from mfrc522 import SimpleMFRC522
import time
class TokenWrapper:
    EVENT_TYPE_CARD_READ = "CARD_READ"
    EVENT_TYPE_DUPLICATE_CARD_READ = "DUPLICATE_CARD_READ"
    EVENT_TYPE_ERROR = "ERROR"

    DUPLICATE_CARD_READ_RESET_AFTER_SECONDS = 5

    def __init__(self):
        self.chip = SimpleMFRC522()
        self.event_handlers = []
        self.events_log = {}
    def handle_event(self, event_type, data):
        if self.event_handlers:
            for e in self.event_handlers:
                e(event_type, data)
    
    def add_event_handler(self, handler):
        self.event_handlers.append(handler)

    def start_monitoring_blocking(self):
        while True:
            try:
                id, text = self.chip.read()
                if id in self.events_log and self.events_log[id] + self.DUPLICATE_CARD_READ_RESET_AFTER_SECONDS > time.time():
                    self.handle_event(self.EVENT_TYPE_DUPLICATE_CARD_READ, (id, text))
                    self.events_log[id] = time.time()
                else:
                    self.handle_event(self.EVENT_TYPE_CARD_READ, (id, text))
                    self.events_log[id] = time.time()
            except Exception as ex:
                self.handle_event(self.EVENT_TYPE_ERROR, ex)
