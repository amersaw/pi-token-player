from token_wrapper import TokenWrapper
from media_player_wrapper import MediaPlayerWrapper
import json
import time
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18,GPIO.OUT)

LED_PIN = 18
def blink_led():
    GPIO.output(LED_PIN, GPIO.HIGH)
    time.sleep(0.05)
    GPIO.output(LED_PIN, GPIO.LOW)
    time.sleep(0.05)
    GPIO.output(LED_PIN, GPIO.HIGH)
    time.sleep(0.05)
    GPIO.output(LED_PIN, GPIO.LOW)
media_player = MediaPlayerWrapper()
cards_mapping_fn = "cards_mapping.json"

def token_event_handler(type, data):
    blink_led()
    if type == TokenWrapper.EVENT_TYPE_CARD_READ:
        id, text = data
        id = str(id)
        print(f"Card read: {id} - {text}")
        with open(cards_mapping_fn) as cards_mapping_file:
            data = json.load(cards_mapping_file)
            if id in data:
                media_player.play_file(data[id])
            else:
                print(f"No mapping for card {id}")
    else:
        print(f"{type}: {data}")

if __name__ == "__main__":
    tw = TokenWrapper()
    tw.add_event_handler(token_event_handler)
    tw.start_monitoring_blocking()
